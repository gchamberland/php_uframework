<?php

require __DIR__.'/../vendor/autoload.php';

use Exception\HttpException;
use Http\JsonResponse;
use Http\Request;
use Http\Response;
use Model\Database\Connection;
use Model\Entity\Status;
use Model\Entity\User;
use Model\Finder\EntityFinder;
use Model\Mapper\EntityMapper;
use Model\EntityManagement\EntityManager;


// Config
$debug = true;

//Database
$dbConnection = NULL;
try{
    $dbConnection = new Connection('mysql:host=127.0.0.1;port=32768;dbname=uframework', 'uframework', 'p4ssw0rd');
} catch (Exception $e){
    die($e->getMessage());
}

//EntityManager
$em = new EntityManager(!$debug);

$app = new \App(new View\TemplateEngine(
    __DIR__.'/templates/'
), $debug);

$app->addListener('process.before', function (Request $request) use ($app)
{
    session_start();

    $allowed = [
        '/login' =>         [ Request::GET, Request::POST ],
        '/register' =>      [ Request::GET, Request::POST ],
        '/message' =>       [ Request::GET, REQUEST::POST ],
        '/message/\d+' =>    [ Request::GET ],
        '/create_form' =>    [ Request::GET ]
    ];

    if (isset($_SESSION['is_authenticated'])
        && true === $_SESSION['is_authenticated']) {
        return;
    }

    foreach ($allowed as $pattern => $methods) {
        if (preg_match(sprintf('#^%s$#', $pattern), $request->getUri())
            && in_array($request->getMethod(), $methods)) {
            return;
        }
    }

    switch ($request->guessBestFormat()) {
        case 'json':
            throw new HttpException(401);
    }

    $_SESSION['target'] = $request->getUri();
    return $app->redirect('/login');
});

/*
 * Index
 */
$app->get('/', function () use ($app)
{
    return $app->render('index.php');
});

$app->get('/login', function (Request $Request) use ($app)
{
    return $app->render('login.php');
});

$app->post('/login', function (Request $request) use ($app, $em, $dbConnection)
{
    $user = $request->getParameter('user');
    $password = $request->getParameter('password');

    $finder = new EntityFinder('Model\Entity\User', $em, $dbConnection);

    $retrieved = $finder->findOneBy(['user' => $user]);

    if (NULL !== $retrieved){
        if ($retrieved->getPassword() === $password) {
            $_SESSION['is_authenticated'] = TRUE;

            $_SESSION['user'] = $retrieved->getName();

            if (isset($_SESSION['target'])){
                return $app->redirect($_SESSION['target']);
            }

            return $app->redirect('/message');
        }
    }

    return $app->render('login.php', [ 'user' => $user ]);
});

$app->get('/logout', function (Request $request) use ($app)
{
    session_destroy();

    return $app->redirect('/message');
});

$app->get('/register', function () use ($app){
    return $app->render('register.php');
});

$app->post('/register', function (Request $request) use ($app, $em, $dbConnection)
{
    $user = $request->getParameter('user', '');
    $password = $request->getParameter('password', '');

    if ('' === $user || '' === $password)
        return $app->render('register.php', ['error' => 'User name and password cannot be empty.']);

    $mapper = new EntityMapper($em, $dbConnection);

    try{
        $mapper->persist(new User($user, $password));
        return $app->redirect('/login');
    }catch (PDOException $e){
        throw new HttpException(500);
    }
});

/*
 * List of all messages
 */
$app->get('/message', function (Request $request) use ($app, $em, $dbConnection)
{
    $criteria = [];
    $filterNames = ['user', 'message', 'client'];

    foreach ($filterNames as $filter){
        $tmp = $request->getParameter($filter);

        if (NULL !== $tmp){
            $criteria[$filter] = $tmp;
        }
    }

    $date = $request->getParameter('date');

    if ($date !== NULL){
        $criteria['date'] = DateTime::createFromFormat('Ymd-His');
    }

    $sort = $request->getParameter('orderBy');

    $finder = new EntityFinder('Model\Entity\Status', $em, $dbConnection);

    $messages = $finder->findAll($criteria, $sort);

    if (0 === count($messages))
        return new Response('No Content', 204);

	if ($request->guessBestFormat() === 'json')
		return new JsonResponse($messages);

    return $app->render('list.php', ['messages' => $messages]);
});

$app->get('/message/(\d+)', function (Request $request, $id) use ($app, $em, $dbConnection)
{

    $finder = new EntityFinder('Model\Entity\Status', $em, $dbConnection);

    $message = $finder->findOneById($id);

    if (NULL === $message) {
        throw new HttpException(404);
    }
    
    if ($request->guessBestFormat() === 'json')
		return new JsonResponse($message);

    return $app->render('message.php', ['message' => $message]);
});

$app->post('/message', function (Request $request) use ($app, $em, $dbConnection)
{
    $message = $request->getParameter('message', '');
    $client = $request->getParameter('client');

    if ('' === $message)
        return new Response('Unprocessable entity', 422);

    $mapper = new EntityMapper($em, $dbConnection);

    $id = $mapper->persist(
        new Status(
            isset($_SESSION['user']) ? $_SESSION['user'] : 'Anonymous',
            $message, new DateTime(),
            NULL,
            $client)
    );

    if ($request->getReferer() == '/create_form') {
        $app->redirect('/message');
    } else {
        return new Response('', 201, ['Location' => '/message/'.$id]);
    }
});

$app->delete('/message/(\d+)', function (Request $request, $id) use ($app, $em, $dbConnection)
{
    $finder = new EntityFinder('Model\Entity\Status', $em, $dbConnection);

    $status = $finder->findOneById($id);

    if (NULL === $finder->findOneById($id)) {
        throw new HttpException(404, 'Not found');
    }

    $mapper = new EntityMapper($em, $dbConnection);
    $mapper->delete($status);

    if (substr($request->getReferer(), 0, 12) === '/delete_form') {
        $app->redirect('/message');
    } else {
        return new Response('No Content', 204);
    }
});

$app->get('/create_form', function () use ($app) {
    if (isset($_SESSION['user'])){
        return $app->render('create_form.php', ['user' => $_SESSION['user']]);
    }

    return $app->render('create_form.php');
});

$app->get('/delete_form/(\d+)', function (Request $request, $id) use ($app)
{
    return $app->render('delete_form.php', ['id' => $id]);
});

return $app;
