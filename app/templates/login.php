<!doctype html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <title>Login</title>
    </head>
    <body>
    <?php include 'nav.php' ?>
    <form action="/login" method="POST">
        <span><p color="red"><?= isset($message) ? $message : '' ?></p></span>
        <label for="user">Username:</label>
        <input id="user" type="text" name="user" value="<?= isset($user) ? $user : '' ?>">
        <label for="password">Password :</label>
        <input type="password" id="password" name="password"/>
        <input type="submit" value="Login!">
    </form>
    </body>
</html>