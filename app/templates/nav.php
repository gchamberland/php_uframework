<nav>
    <a href="/create_form">Add new</a>
    <?php
        if (!isset($_SESSION['is_authenticated'])){
            echo '<a href="/login">Login</a>'."\n";
            echo '<a href="/register">Register</a>'."\n";
        } else {
            echo '<a href="/logout">Logout</a>'."\n";
        }
    ?>
</nav>