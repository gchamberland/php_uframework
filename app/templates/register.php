<!doctype html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <title>Registration</title>
    </head>
    <body>
    <?php include 'nav.php' ?>
    <form action="/register" method="POST">
        <span><p color="red"><?= isset($error) ? $error : '' ?></p></span>
        <label for="user">Username :</label>
        <input id="user" type="text" name="user">
        <label for="password">Password :</label>
        <input type="password" id="password" name="password"/>
        <input type="submit" value="Register!">
    </form>
    </body>
</html>