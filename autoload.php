<?php


$autoloader = function ($className) {

    $reps = array('/src', '/tests', null);

    $cacheFilePath = __DIR__.'/cache.php';
    $cache = [];

    if (file_exists($cacheFilePath)) {
        include $cacheFilePath;

        if (isset($cache[$className])) {
            require_once $cache[$className];

            return true;
        }
    } else {
        $cache = array();
    }

    $classPath = $cache[$className] = '/'.str_replace('_', '/',
        str_replace('\\', '/', $className)).'.php';

    foreach ($reps as $rep) {
        if ($rep == null) {
            return;
        }

        if (file_exists(__DIR__.$rep.$classPath)) {
            $cache[$className] = __DIR__.$rep.$classPath;
            require_once $cache[$className];
            break;
        }
    }

    $cacheFile = fopen($cacheFilePath, 'w');

    if ($cacheFile === false) {
        echo 'Failed to write cache in file "'.$cacheFilePath.'".';

        return false;
    }

    fwrite($cacheFile, "<?php\n\n\$cache = ".var_export($cache, true).";\n");
    fclose($cacheFile);
};

spl_autoload_register($autoloader);
