<?php
/**
 * Created by PhpStorm.
 * User: greg
 * Date: 14/02/16
 * Time: 18:53
 */

namespace Exception;


use Exception;

class BrokenEntityException extends \Exception
{
    public function __construct($message, $code = NULL, Exception $previous = NULL)
    {
        parent::__construct($message, $code, $previous);
    }
}