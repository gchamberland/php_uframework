<?php

namespace Http;

use \Negotiation\Negotiator;

class Request
{
    const GET = 'GET';

    const POST = 'POST';

    const PUT = 'PUT';

    const DELETE = 'DELETE';
    
    private $negotiator;

    private $method;

    private $uri;

    private $parameters;

    public static function createFromGlobals() : Request
    {
		if (self::readContentType() === 'application/json') {
			$data    = file_get_contents('php://input');
			$request = @json_decode($data, true);

			return new self($_GET, $request === null ? [] : $request);
		}

        return new self($_GET, $_POST);
    }

    public function getReferer() : string
    {
        if (isset($_SERVER['HTTP_REFERER'])) {
            if (preg_match('#^http(s|)://[^/]*?(/.*?)$#', $_SERVER['HTTP_REFERER'], $matches)){
                return $matches[2];
            }
        }
        return '';
    }

    public function getMethod() : string
    {
        if (self::POST === $this->method) {
            return $this->getParameter('_method', $this->method);
        }

        return $this->method;
    }

    public function getUri() : string
    {
        return $this->uri;
    }

    public function getParameter($name, $default = NULL)
    {
        if (false === isset($this->parameters[$name])) {
            return $default;
        }

        return $this->parameters[$name];
    }
    
    public function guessBestFormat() : string
    {
		$mediaType = $this->negotiator->getBest($_SERVER['HTTP_ACCEPT'], array('text/html', 'application/json'));
		$typeMIME = $mediaType->getValue();
		
		if ($typeMIME === 'application/json')
			return 'json';
		else
			return 'html';
	}
	
	private static function readContentType() : string
    {
		$ct = null;
		if (isset($_SERVER['HTTP_CONTENT_TYPE'])) {
			$ct = $_SERVER['HTTP_CONTENT_TYPE'];
		} else if (isset($_SERVER['CONTENT_TYPE'])) {
			$ct = $_SERVER['CONTENT_TYPE'];
		}
		
		return preg_split('#;#', $ct, 2)[0];
	}

    private function __construct(array $query = array(), array $request = array())
    {
        $this->parameters = array_merge($query, $request);
        $this->method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : self::GET;
        $this->uri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '/';

        if ($pos = strpos($this->uri, '?')) {
            $this->uri = substr($this->uri, 0, $pos);
        }
        
        $this->negotiator = new Negotiator();
    }
}
