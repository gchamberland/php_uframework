<?php
/**
 * Created by PhpStorm.
 * User: greg
 * Date: 08/02/16
 * Time: 22:17
 */

namespace Model\Database;

use PDO;

class Connection extends PDO
{
    private function prepareAndBind(String $query, array $parameters){
        $stmt = $this->prepare($query);

        foreach ($parameters as $name => $value) {
            $stmt->bindValue(':' . $name, $value);
        }

        return $stmt;
    }

    /**
     * Connection constructor.
     * @param $dsn
     * @param $username
     * @param $passwd
     */
    public function __construct($dsn, $username, $passwd)
    {
        parent::__construct($dsn, $username, $passwd, [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ]);
    }

    /**
     * @param string $query
     * @param array  $parameters
     *
     * @return bool Returns `true` on success, `false` otherwise
     */
    public function executeQuery($query, array $parameters = [])
    {
        return $this->prepareAndBind($query, $parameters)->execute();
    }

    /**
     * @param String $query
     * @param array $parameters
     * @return array|null
     */
    public function getQueryResults(String $query, array $parameters = [])
    {
        $stmt = $this->prepareAndBind($query, $parameters);

        if ($stmt->execute()) {
            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        }

        return NULL;
    }

    /**
     * @param String $query
     * @param array $parameters
     * @return mixed|null
     */
    public function getQuerySingleResult(String $query, array $parameters = [])
    {
        $stmt = $this->prepareAndBind($query, $parameters);

        if ($stmt->execute() && $stmt->rowCount() > 0) {
            return $stmt->fetch(PDO::FETCH_ASSOC);
        }

        return NULL;
    }
}