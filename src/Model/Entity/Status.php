<?php
/**
 * Created by PhpStorm.
 * User: greg
 * Date: 09/02/16
 * Time: 08:53
 */

namespace Model\Entity;

/**
 * Class Status
 * @package Model\Entity
 *
 */
class Status implements \JsonSerializable
{
    /**
     * @var long|NULL
     *
     * @Id
     * @FilterableBy = :id
     * @SortableBy
     */
    private $id;

    /**
     * @var string
     *
     * @FilterableBy = :user
     * @SortableBy
     */
    private $user;

    /**
     * @var string
     *
     * @FilterableBy LIKE :message
     */
    private $message;

    /**
     * @var \DateTime
     * @Date
     * @SortableBy
     */
    private $date;

    /**
     * @var NULL|string
     *
     * @FilterableBy = :client
     * @SortableBy
     */
    private $client;

    /**
     * Status constructor.
     * @param string $user
     * @param string $message
     * @param \DateTime $date
     * @param string|NULL $id
     * @param string|NULL $client
     */
    public function __construct(string $user, string $message, \DateTime $date, string $id = NULL,  string $client = NULL)
    {
        $this->id = $id;
        $this->user = $user;
        $this->message = $message;
        $this->date = $date;
        $this->client = $client;
    }

    /**
     * @return long
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return string
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    function jsonSerialize()
    {
        return [
            'id' => $this->getId(),
            'user' => $this->getUser(),
            'message' => $this->getMessage(),
            'date' => $this->getDate(),
            'client' => $this->getClient()
        ];
    }
}