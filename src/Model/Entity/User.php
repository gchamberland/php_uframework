<?php
/**
 * Created by PhpStorm.
 * User: greg
 * Date: 14/02/16
 * Time: 15:13
 */

namespace Model\Entity;


class User
{
    /**
     * @var long
     *
     * @Id
     */
    private $id;

    /**
     * @var string
     *
     * @FilterableBy = :user
     */
    private $name;

    /**
     * @var string
     */
    private $password;

    /**
     * User constructor.
     * @param $id
     * @param $name
     * @param $password
     */
    public function __construct(string $name, string $password, string $id = NULL)
    {
        $this->id = $id;
        $this->name = $name;
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }


}