<?php

/**
 * Created by PhpStorm.
 * User: greg
 * Date: 13/02/16
 * Time: 19:08
 */
namespace Model\EntityManagement;

class EntityManager
{
    const CACHE_FILE_DIR = __DIR__.'/../../../app/cache';
    const CACHE_FILE_NAME = '/entity_cache.php';

    const DATE_FORMAT = 'Y-m-d H:i:s';

    private $useMetaModelCache;

    private $cache;

    private function readEntityCacheFile(){
        $cacheFilePath = self::CACHE_FILE_DIR.self::CACHE_FILE_NAME;

        $cache = [];

        if (file_exists($cacheFilePath)) {
            $tmp = require $cacheFilePath;

            if (is_array($tmp)){
                $cache = $tmp;
            }
        }

        return $cache;
    }

    private function writeEntityCacheFile($cache) {
        if (!file_exists(self::CACHE_FILE_DIR)) {
            mkdir(self::CACHE_FILE_DIR, 0755, TRUE);
        }

        file_put_contents(self::CACHE_FILE_DIR.self::CACHE_FILE_NAME,
            "<?php\r\n\r\n return ".var_export($cache, true).";\r\n");
    }

    public function getMetaModel($entityType){

        if (NULL === $this->cache) {
            $this->cache = $this->readEntityCacheFile();
        }

        $rClass = new \ReflectionClass($entityType);
        $entityName = $rClass->getName();

        if (!isset($this->cache[$entityName]) || !$this->useMetaModelCache){
            $this->cache[$entityName] = (new MetaModelBuilder())->buildMetaModel($rClass);
            $this->writeEntityCacheFile($this->cache);
        }

        return $this->cache[$entityName];
    }

    public function entityToUpdateParameters($entity){
        $meta = $this->getMetaModel($entity);
        $class = new \ReflectionClass($entity);
        $params = [];

        foreach ($meta['getters'] as $prop => $getter){
            $propGetter = $class->getMethod($getter);
            $value = $propGetter->invoke($entity);

            if ($value instanceof \DateTime){
                $value = $value->format(self::DATE_FORMAT);
            }

            $params[$prop] = $value;
        }

        return $params;
    }

    public function entityToInsertParameters($entity){
        $params = $this->entityToUpdateParameters($entity);
        unset($params[$this->getIdPropName($entity)]);

        return $params;
    }

    public function entityToDeleteParameters($entity){
        return [$this->getIdPropName($entity) => $this->getId($entity)];
    }

    public function paramsForSelectQuery($entityType, $criteria){
        $params = [];
        $meta = $this->getMetaModel($entityType);
        $filterNames = $meta['filterNames'];

        foreach ($criteria as $name => $value){
            if (in_array($name, $filterNames)){
                $params[$name] = $value;
            }
        }

        return $params;
    }

    public function resultRowToEntity($entityType, $row){
        $class = new \ReflectionClass($entityType);
        $dateProps = $this->getMetaModel($entityType)['dates'];

        $args = array();

        foreach ($dateProps as $prop){
            $row[$prop] = new \DateTime($row[$prop]);
        }

        foreach ($class->getConstructor()->getParameters() as $param){
            array_push($args, $row[$param->getName()]);
        }

        return $class->newInstanceArgs($args);
    }

    public function getSorts($entityType){
        return $this->getMetaModel($entityType)['sorts'];
    }

    public function getId($entity){
        $meta = $this->getMetaModel($entity);

        return (new \ReflectionClass($entity))
                ->getMethod($meta['getters'][$meta['idProp']])
                ->invoke($entity);
    }

    public function getIdPropName($entityType){
        return $this->getMetaModel($entityType)['idProp'];
    }

    public function getInsertQuery($entityType){
        return $this->getMetaModel($entityType)['insertQuery'];
    }

    public function getSelectQuery($entityType, $criteria = [], $sort = NULL){
        $queryBody = '';
        $meta = $this->getMetaModel($entityType);
        $filterNames = $meta['filterNames'];
        $filterExpressions = $meta['filterExpressions'];

        foreach ($criteria as $name => $value){
            if (in_array($name, $filterNames)){
                if ($queryBody != '') {
                    $queryBody .= ' AND';
                }

                $queryBody .= ' '.$filterExpressions[$name];
            }
        }

        if ($queryBody !== ''){
            $queryBody = ' WHERE'.$queryBody;
        }

        if (in_array($sort, $meta['sorts'])){
            $queryBody .= ' ORDER BY '.$sort;
        }

        return $meta['selectQuery'].$queryBody;
    }

    public function getUpdateQuery($entityType){
        return $this->getMetaModel($entityType)['updateQuery'];
    }

    public function getDeleteQuery($entityType){
        return $this->getMetaModel($entityType)['deleteQuery'];
    }

    public function  __construct($useMetaModelCache = TRUE){
        $this->useMetaModelCache = $useMetaModelCache;
        $this->cache = NULL;
    }
}