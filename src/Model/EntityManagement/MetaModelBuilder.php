<?php
/**
 * Created by PhpStorm.
 * User: greg
 * Date: 12/02/16
 * Time: 17:00
 */

namespace Model\EntityManagement;

use Exception\BrokenEntityException;

class MetaModelBuilder
{
    const ENTITY_ANNOTATION_NAMES = 'Table';
    const PROP_ANNOTATION_NAMES = 'Id|FilterableBy|Transient|SortableBy|Format|Column|Date|DateFormat';
    const ONCE_PER_PROP_ANNOTATIONS = array('Id', 'Transient', 'SortableBy', 'DateFormat', 'Column', 'Date', 'DateFormat');

    private function checkForDuplicatedPropAnnotations($propName, $propAnnotations){

        foreach ($propAnnotations as $key => $value){
            if (in_array($key, self::ONCE_PER_PROP_ANNOTATIONS))
                if (is_array($value))
                    throw new BrokenEntityException('Duplicated annotation "'.$key.'" for property "'.$propName.'"');
        }
    }

    private function parseAnnotations(string $comments, string $annotationNames){
        $annotations = [];

        preg_match_all('#@(['.$annotationNames.'].*?)(\h+(.*?)|())\R#s', $comments, $matches, PREG_SET_ORDER);

        foreach ($matches as $result) {

            if (isset($annotations[$result[1]])){
                if (!is_array($annotations[$result[1]])) {
                    $annotations[$result[1]] = array($annotations[$result[1]]);
                }
                $annotations[$result[1]][] = $result[3];
                continue;
            }

            $annotations[$result[1]] = $result[3];
        }

        return $annotations;
    }

    private function getPropertyColumnName($propertyName, $propAnnotations){
        if (isset($propAnnotations['Column'])) {
            if ($propAnnotations['Column'] === '')
                throw new BrokenEntityException('Missing column name in @Column annotation for property "'.$propertyName.'"');

            return $propAnnotations['Column'];
        }

        return strtolower($propertyName);
    }

    private function getPropertyFilters($propName, $propAnnotations, &$metaModel){
        if (!isset($propAnnotations['FilterableBy'])){
            return;
        }

        if (!is_array($propAnnotations['FilterableBy']))
            $propAnnotations['FilterableBy'] = [$propAnnotations['FilterableBy']];

        foreach ($propAnnotations['FilterableBy'] as $expr){
            $matches = [];
            preg_match('#[^:]*?(:\w+).*?#', $expr, $matches);

            if (!isset($matches[1])) {
                throw new BrokenEntityException('Missing filter value in @FilterableBy annotation\'s expression "'
                    .$expr.'" for property "'
                    . $propName . '" (Correct syntax example : @FilterableBy <= :value)');
            }


            $filterName = substr($matches[1], 1);

            if (isset($metaModel['filterExpressions'][$filterName])){
                throw new BrokenEntityException('Duplicate filter name "'.$filterName.'"');
            }

            $metaModel['filterNames'][] = $filterName;
            $metaModel['filterExpressions'][$filterName] = $metaModel['columns'][$propName].' '.$expr;
        }
    }

    private function getPropType(string $propName, array $constructorParams)
    {
        foreach ($constructorParams as $param) {
            if ($param->getName() === $propName) {
                $paramClass = $param->getClass();
                return $paramClass === NULL ? '' : $paramClass->getName();
            }
        }

        throw new BrokenEntityException('Missing constructor parameter for property "' . $propName . '"');
    }

    private function getGetterName($propName, $class){
        try {
            $name = 'get' . strtoupper(substr($propName, 0, 1)) . substr($propName, 1);
            $class->getMethod($name);
            return $name;
        }
        catch (\ReflectionException $e){
            throw new BrokenEntityException('Missing getter for property "'.$propName.'"');
        }
    }

    private function getPropInfo(\ReflectionClass $class, &$metaModel){
        $properties = $class->getProperties();
        $metaModel['idProp'] = NULL;

        $metaModel['sorts'] = [];
        $metaModel['dates'] = [];
        $metaModel['filters'] = [];
        $metaModel['formats'] = [];
        $metaModel['types'] = [];

        $constructorParams = $class->getConstructor()->getParameters();

        foreach ($properties as $property) {

            $propName = $property->getName();
            $annotations = $this->parseAnnotations($property->getDocComment(), self::PROP_ANNOTATION_NAMES);

            $this->checkForDuplicatedPropAnnotations($propName, $annotations);

            if (isset($annotations['Transient'])) {
                continue;
            }

            $metaModel['columns'][$propName] = $this->getPropertyColumnName($propName, $annotations);

            self::getPropertyFilters($propName, $annotations, $metaModel);

            if (isset($annotations['SortableBy'])) {
                $metaModel['sorts'][] = $propName;
            }

            if (isset($annotations['Date'])){
                $metaModel['dates'][] = $propName;
            }

            $metaModel['types'][$propName] =  $this->getPropType($propName, $constructorParams);

            if (isset($annotations['DateFormat'])) {
                if ($metaModel['types'][$propName] !== 'DateTime') {
                    throw new BrokenEntityException('@DateFormat annotation defined on non DateTime property "' . $propName . '"');
                }
                $metaModel['formats'][$propName] = $annotations['Format'];
            }

            if (isset($annotations['Id'])) {
                if ($metaModel['idProp'] !== NULL) {
                    throw new BrokenEntityException('Duplicated annotation @Id : properties "'
                        . $metaModel['idProp'] . '" and "' . $propName . '"');
                }
                $metaModel['idProp'] = $propName;
            }

            $metaModel['getters'][$propName] = $this->getGetterName($propName, $class);
        }

        if ($metaModel['idProp'] === NULL) {
            throw new BrokenEntityException('Missing annotation @Id in entity');
        }
    }

    private function getTableName(string $className, $annotations){
        if (isset($annotations['Table'])) {
            if (is_array($annotations['Table'])) {
                throw new BrokenEntityException('Duplicated annotation @Table');
            }

            if ($annotations['Table'] === '')
                throw new BrokenEntityException('Empty table name in annotation @Table');

            return $annotations['Table'];
        }

        return strtolower($className);
    }

    private function buildInsertQuery(&$metaModel){
        $insertQuery = 'INSERT INTO '. $metaModel['tableName'] .' (';
        $tmp = '';

        foreach ($metaModel['columns'] as $prop => $column){
            if ($tmp !== ''){
                $tmp .= ', ';
            }

            if ($metaModel['idProp'] !== $prop) {
                $tmp .= $column;
            }
        }

        $insertQuery .= $tmp.') VALUES (';
        $tmp = '';

        foreach ($metaModel['columns'] as $prop => $column){
            if ($tmp !== ''){
                $tmp .= ', ';
            }

            if ($metaModel['idProp'] !== $prop) {
                $tmp .= ':'.$prop;
            }
        }

       $metaModel['insertQuery'] = $insertQuery.$tmp.')';
    }

    private function buildUpdateQuery(&$metaModel){
        $updateQuery = 'UPDATE '. $metaModel['tableName'] .' SET ';
        $tmp = '';

        foreach ($metaModel['columns'] as $prop => $column){
            if ($tmp !== ''){
                $tmp .= ', ';
            }

            if ($metaModel['idProp'] !== $prop) {
                $tmp .= $column.' = :'.$prop;
            }
        }

        $metaModel['updateQuery'] = $updateQuery.$tmp.') WHERE '.$metaModel['columns'][$metaModel['idProp']].' = :'.$metaModel['idProp'];
    }

    private function buildDeleteQuery(&$metaModel){

        $metaModel['deleteQuery'] = 'DELETE FROM '.$metaModel['tableName'].' WHERE '
            .$metaModel['columns'][$metaModel['idProp']].' = :'.$metaModel['idProp'];
    }

    private function buildSelectQuery(&$metaModel){
        $selectQuery = 'SELECT ';
        $tmp = '';

        foreach ($metaModel['columns'] as $prop => $column){
            if ($tmp !== ''){
                $tmp .= ', ';
            }

            $tmp .= $column.' AS '.$prop;
        }

        $metaModel['selectQuery'] = $selectQuery.$tmp.' FROM '.$metaModel['tableName'];
    }

    /** Build the meta model of an entity.
     * @param $class The class's name of the entity, or an entity object.
     * @return array The entity's meta model
     * @throws \Exception If the entity's meta model contains errors.
     * @throws \ReflectionException If the class given cannot be found.
     */
    public function buildMetaModel(\ReflectionClass $class){

        try {
            $metaModel = [];

            $annotations = $this->parseAnnotations($class->getDocComment(), self::ENTITY_ANNOTATION_NAMES);

            $metaModel['tableName'] = $this->getTableName($class->getShortName(), $annotations);

            $this->getPropInfo($class, $metaModel);
            $this->buildInsertQuery($metaModel);
            $this->buildSelectQuery($metaModel);
            $this->buildUpdateQuery($metaModel);
            $this->buildDeleteQuery($metaModel);

            return $metaModel;
        }
        catch (BrokenEntityException $e){
            throw new BrokenEntityException('Error reading meta model of entity "'
                .$class->getName().'" : '.$e->getMessage().'');
        }
    }
}