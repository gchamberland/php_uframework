<?php

/**
 * Created by PhpStorm.
 * User: greg
 * Date: 13/02/16
 * Time: 19:07
 */

namespace Model\Finder;

use Model\Database\Connection;
use Model\EntityManagement\EntityManager;

class EntityFinder implements FinderInterface
{
    private $em;
    private $con;
    private $entityType;

    public function __construct($entityType, EntityManager $em, Connection $con){
        $this->em = $em;
        $this->con = $con;
        $this->entityType = $entityType;
    }

    /**
     * @param array $criteria
     * @param null $sort
     * @return bool
     */
    public function findAll($criteria = [], string $sort = NULL)
    {
        $entities = [];

        $results =  $this->con->getQueryResults(
            $this->em->getSelectQuery($this->entityType, $criteria, $sort),
            $this->em->paramsForSelectQuery($this->entityType, $criteria)
        );

        foreach ($results as $row){
            $entities[] = $this->em->resultRowToEntity($this->entityType, $row);
        }

        return $entities;
    }

    /**
     * Retrieve an element by its id.
     *
     * @param mixed $id
     *
     * @return null|mixed
     */
    public function findOneById($id)
    {
        $result =  $this->con->getQuerySingleResult(
            $this->em->getSelectQuery($this->entityType)
                .' WHERE '.$this->em->getIdPropName($this->entityType).' = :id',
            ['id' => $id]
        );

        return NULL === $result ? NULL : $this->em->resultRowToEntity($this->entityType, $result);
    }

    /**
     * Retrieve an element by defined by this criteria.
     *
     * @param mixed $id
     *
     * @return null|mixed
     */
    public function findOneBy($criteria)
    {
        $result =  $this->con->getQuerySingleResult(
            $this->em->getSelectQuery($this->entityType, $criteria),
            $this->em->paramsForSelectQuery($this->entityType, $criteria)
        );

        return NULL === $result ? NULL : $this->em->resultRowToEntity($this->entityType, $result);
    }
}