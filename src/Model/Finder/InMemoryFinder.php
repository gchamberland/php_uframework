<?php

namespace Model\Finder;

class InMemoryFinder implements FinderInterface
{
    private $messages = array('1' => 'Message 1', '2' => 'message2', '3' => 'Message3');

    /**
     * Returns all elements.
     *
     * @return array
     */
    public function findAll()
    {
        return $this->messages;
    }

    /**
     * Retrieve an element by its id.
     *
     * @param mixed $id
     *
     * @return null|mixed
     */
    public function findOneById($id)
    {
        return $this->messages[$id] ?? null;
    }
}
