<?php

namespace Model\Finder;

use Exception\HttpException;

class JsonFinder implements FinderInterface
{
    const DATA_FOLDER = '/../../data/';
    const DATA_FILE_NAME = 'statuses';
    const DATA_FILE_EXTENSION = 'json';

    public function create($user, $message)
    {
        $data = $this->readDataFile();

        end($data);
        $id = key($data) + 1;
        $data[$id] = ['user' => $user, 'message' => $message];

        $this->writeDataFile($data);

        return $id;
    }

    /**
     * Returns all elements.
     *
     * @return array
     */
    public function findAll()
    {
        $data = $this->readDataFile();

        return $data;
    }

    /**
     * Retrieve an element by its id.
     *
     * @param mixed $id
     *
     * @return null|mixed
     */
    public function findOneById($id)
    {
        $data = $this->readDataFile();

        return $data[$id];
    }

    public function update($id, $message)
    {
        $data = $this->readDataFile();

        $this->data[$id]['message'] = $message;

        $this->writeDataFile($data);
    }

    public function delete($id)
    {
        $data = $this->readDataFile();

        unset($data[$id]);

        $this->writeDataFile($data);
    }

    private function readDataFile()
    {
        if (false === file_exists(__DIR__.self::DATA_FOLDER.self::DATA_FILE_NAME.'.'.self::DATA_FILE_EXTENSION)) {
            $this->writeDataFile([]);
        }

        $fileContent = file_get_contents(__DIR__.self::DATA_FOLDER.self::DATA_FILE_NAME.'.'.self::DATA_FILE_EXTENSION);

        if (false === $fileContent) {
            throw new HttpException(500, 'Data file reading impossible.');
        }

        $data = json_decode($fileContent, true);

        if (null === $data) {
            throw new HttpException(500, 'Data file is corrupted.');
        }

        return $data;
    }

    private function writeDataFile($data)
    {
        if (false === file_exists(__DIR__.self::DATA_FOLDER)) {
            if (false === mkdir(__DIR__.self::DATA_FOLDER, 0700, true)) {
                throw new HttpException(500, 'Data directory creation is impossible.');
            }
        }

        if (false === file_put_contents(__DIR__.self::DATA_FOLDER.self::DATA_FILE_NAME.'.'.self::DATA_FILE_EXTENSION, json_encode($data), LOCK_EX)) {
            throw new HttpException(500, 'Data file writing is impossible.');
        }
    }
}
