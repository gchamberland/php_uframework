<?php

/**
 * Created by PhpStorm.
 * User: greg
 * Date: 13/02/16
 * Time: 19:06
 */

namespace Model\Mapper;

use Model\Database\Connection;
use Model\EntityManagement\EntityManager;

class EntityMapper
{
    private $em;
    private $con;

    private function update($entity)
    {
        return $this->con->executeQuery(
            $this->em->getUpdateQuery($entity),
            $this->em->entityToUpdateParameters($entity)
        );
    }

    private function insert($entity)
    {
        return $this->con->executeQuery(
            $this->em->getInsertQuery($entity),
            $this->em->entityToInsertParameters($entity)
        );
    }

    public function __construct(EntityManager $em, Connection $con){
        $this->em = $em;
        $this->con = $con;
    }

    public function persist($entity)
    {
        if (NULL === $this->em->getId($entity)){
            if ($this->insert($entity)){
                return $this->con->lastInsertId();
            }

            return false;
        }

        return $this->update($entity);
    }

    public function delete($entity){
        return $this->con->executeQuery(
            $this->em->getDeleteQuery($entity),
            $this->em->entityToDeleteParameters($entity)
        );
    }
}