<?php

/**
 * Created by PhpStorm.
 * User: greg
 * Date: 14/02/16
 * Time: 18:49
 */
use Exception\BrokenEntityException;

class MetaModelBuilderTest extends TestCase
{
    private $builder;

    public function setUp()
    {
        $this->builder = new \Model\EntityManagement\MetaModelBuilder();
    }

    /**
     * @expectedException Exception\BrokenEntityException
     */
    public function testTwoIdPropDetection()
    {
        $this->builder->buildMetaModel(new ReflectionClass(new \fixtures\BrokenEntity\TwoIdPropEntity()));
    }

    /**
     * @expectedException Exception\BrokenEntityException
     */
    public function testMissingTableNameDetection()
    {
        $this->builder->buildMetaModel(new ReflectionClass(new \fixtures\BrokenEntity\MissingTableNameEntity()));
    }

    /**
     * @expectedException Exception\BrokenEntityException
     */
    public function testMissingIdDetection()
    {
        $this->builder->buildMetaModel(new ReflectionClass(new \fixtures\BrokenEntity\MissingIdEntity()));
    }

    /**
     * @expectedException Exception\BrokenEntityException
     */
    public function testMissingGetterDetection()
    {
        $this->builder->buildMetaModel(new ReflectionClass(new \fixtures\BrokenEntity\MissingGetterEntity()));
    }

    /**
     * @expectedException Exception\BrokenEntityException
     */
    public function testMissingFilterValueDetection()
    {
        $this->builder->buildMetaModel(new ReflectionClass(new \fixtures\BrokenEntity\MissingFilterValueEntity()));
    }

    /**
     * @expectedException Exception\BrokenEntityException
     */
    public function testMissingConstructorParameterDetection()
    {
        $this->builder->buildMetaModel(new ReflectionClass(new \fixtures\BrokenEntity\MissingConstructorParameterEntity()));
    }

    /**
     * @expectedException Exception\BrokenEntityException
     */
    public function testMissingColumnNameDetection()
    {
        $this->builder->buildMetaModel(new ReflectionClass(new \fixtures\BrokenEntity\MissingColumnNameEntity()));
    }

    /**
     * @expectedException Exception\BrokenEntityException
     */
    public function testDuplicatedFilterNameDetection()
    {
        $this->builder->buildMetaModel(new ReflectionClass(new \fixtures\BrokenEntity\DuplicatedFilterNameEntity()));
    }

    /**
     * @expectedException Exception\BrokenEntityException
     */
    public function testDuplicatedAnnotationDetection()
    {
        $this->builder->buildMetaModel(new ReflectionClass(new \fixtures\BrokenEntity\DuplicatedAnnotationEntity()));
    }

    /**
     * @expectedException Exception\BrokenEntityException
     */
    public function testDateFormatOnNonDateTimeDetection()
    {
        $this->builder->buildMetaModel(new ReflectionClass(new \fixtures\BrokenEntity\DateFormatOnNoDateTimeEntity()));
    }

    public function testMetaModelColumnsDescription(){
        $meta = $this->builder->buildMetaModel(new ReflectionClass(new \fixtures\GoodEntity()));
        $this->assertEquals($meta['column'], ['id' => 'id_entity', 'date' => 'date']);
    }

    public function testMetaModelfiltersExpressionsDescription(){
        $meta = $this->builder->buildMetaModel(new ReflectionClass(new \fixtures\GoodEntity()));
        $this->assertEquals($meta['filterExpressions'], [0 => 'id_entity < :test', 1 => 'id_entity > :test2']);
    }

    public function testMetaModelFilterNamesDescription(){
        $meta = $this->builder->buildMetaModel(new ReflectionClass(new \fixtures\GoodEntity()));
        $this->assertEquals($meta['filterNames'], [0 => 'test', 1 => 'test2']);
    }
}