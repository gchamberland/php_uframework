<?php
/**
 * Created by PhpStorm.
 * User: greg
 * Date: 14/02/16
 * Time: 18:47
 */

namespace fixtures\BrokenEntity;


class DateFormatOnNoDateTimeEntity
{
    /**
     * @var
     *
     * @Id
     * @DateFormat hhiiss
     */
    private $id;

    /**
     * @var
     *
     */
    private $id2;

    public function __construct($id = NULL, $id2 = NULL){
        $this->id = $id;
        $this->id2 = $id2;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getId2()
    {
        return $this->id2;
    }
}