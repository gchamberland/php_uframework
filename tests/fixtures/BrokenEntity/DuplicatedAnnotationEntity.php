<?php
/**
 * Created by PhpStorm.
 * User: greg
 * Date: 14/02/16
 * Time: 18:34
 */

namespace fixtures\BrokenEntity;


class DuplicatedAnnotationEntity
{
    /**
     * @var
     *
     * @Id
     * @Id
     */
    private $id;

    public function __construct($id = NULL){
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


}