<?php
/**
 * Created by PhpStorm.
 * User: greg
 * Date: 14/02/16
 * Time: 18:46
 */

namespace fixtures\BrokenEntity;


class DuplicatedFilterNameEntity
{
    /**
     * @var
     *
     * @Id
     *
     * @FilterableBy = :duplicated
     */
    private $id;

    /**
     * @var
     *
     * @FilterableBy <= :duplicated
     */
    private $id2;

    public function __construct($id = NULL, $id2 = NULL){
        $this->id = $id;
        $this->id2 = $id2;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getId2()
    {
        return $this->id2;
    }
}