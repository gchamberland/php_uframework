<?php
/**
 * Created by PhpStorm.
 * User: greg
 * Date: 14/02/16
 * Time: 18:40
 */

namespace fixtures\BrokenEntity;


class MissingConstructorParameterEntity
{
    /**
     * @var
     *
     * @Id
     */
    private $id;

    /**
     * @var
     *
     */
    private $name;

    public function __construct($id = NULL){
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

}