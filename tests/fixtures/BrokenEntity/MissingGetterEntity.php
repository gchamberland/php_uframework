<?php
/**
 * Created by PhpStorm.
 * User: greg
 * Date: 14/02/16
 * Time: 18:39
 */

namespace fixtures\BrokenEntity;


class MissingGetterEntity
{
    /**
     * @var
     *
     * @Id
     */
    private $id;

    /**
     * @var
     *
     */
    private $name;

    public function __construct($id = NULL, $name = NULL){
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
}