<?php
/**
 * Created by PhpStorm.
 * User: greg
 * Date: 14/02/16
 * Time: 18:36
 */

namespace fixtures\BrokenEntity;


class TwoIdPropEntity
{
    /**
     * @var
     *
     * @Id
     */
    private $id;

    /**
     * @var
     *
     * @Id
     */
    private $id2;

    public function __construct($id = NULL, $id2 = NULL){
        $this->id = $id;
        $this->id2 = $id2;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getId2()
    {
        return $this->id2;
    }


}