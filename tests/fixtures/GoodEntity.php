<?php
/**
 * Created by PhpStorm.
 * User: greg
 * Date: 14/02/16
 * Time: 19:25
 */

namespace fixtures;

/**
 * Class GoodEntity
 * @package fixtures
 *
 * @Table entity
 */
class GoodEntity
{
    /**
     * @var
     *
     * @Id
     * @Column id_entity
     * @FilterableBy < :test
     * @FilterableBy > :test2
     */
    private $id;

    /**
     * @var
     *
     * @Date
     * @DateFormat YYmmDD
     */
    private $date;

    /**
     * @var
     *
     * @Transient
     */
    private $transient;

    public function __construct($id = NULL, DateTime $date){
        $this->id = $id;
        $this->date = $date;
        $this->transient = 'test';
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }
}